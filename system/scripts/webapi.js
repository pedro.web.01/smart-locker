//==========================================================================;
//      Copyright by Menno Dep. Engenharia - eletronica@menno.com.br        ;
//                   Daniel Galvan de Lima - daniel@dgldesign.com.br        ;
//|-------------------------------------------------------------------------;
//| Todos os direitos reservados, para uso exclusivo em produtos Menno      ;
//==========================================================================;
//| Produto: Menno Wise System                                              ;
//|-------------------------------------------------------------------------;
const stVersion = "3C"                                                      
//==========================================================================;
//|00/00/00|3C| API para controle interno dos sistemas Menno Wise           ;
//|-------------------------------------------------------------------------;
//|                                                                         ;
//==========================================================================;

var ptCallBack = null;
var glwsCtrRes = null;
var glwsCtrRej = null;

var wsCtr = null;
var wsPrn = null;
var wsRet = null;

var wsQry = [];

function SysProtect(){
  window.addEventListener('contextmenu', function (e) { e.preventDefault(); }, false);
}

async function SysWebSocket( pmCallBack) {

  ptCallBack = pmCallBack;

  //#region - Conecta na controladora |------------------------------------

    lnkCtr = "ws"+window.location.href.slice(4)+"Ctr";
    wsCtr = new WebSocket(lnkCtr);

    wsCtr.onopen = async () => {
      if( glwsCtrRes != null) {
        await glwsCtrRes();
        glwsCtrRes = null;
        glwsCtrRej = null;
      }      
    };
    
    wsCtr.onerror = async (err) => {
      console.log(`[SysInfo] Control Error:\n${err}`);
      
      if( glwsCtrRej != null) {
        await glwsCtrRej();
        glwsCtrRes = null;
        glwsCtrRej = null;
       }

      if( wsRet != null ) {      
        wsRet.Rslv({Call:`NAK`, Args:`Error: ${err}`});
        wsRet=null; 
        return;       
      }

    };
    
    wsCtr.onmessage = async (evt) => {

      //console.log(`Control Receive: ${evt.data}`);

      const pmBody = JSON.parse(evt.data);

      switch(pmBody.Call) {

        case "RESTART": {
          window.location.reload(true);
          break;
        }

        case "ERROR": {
          alert(pmBody.Args);
          break;
        }

        case "CLOG": {
          console.log(`[SysInfo] ${pmBody.Args}`);
          break;
        }

        default:{

          if( wsRet == null ) {
            await ptCallBack(pmBody);
          }

          else if( (pmBody.Call == wsRet.Call) || (pmBody.Call == wsRet.Args) || (pmBody.Call == "NAK") ) {
            wsRet.Body = pmBody;
          } 
          
          else {
            await ptCallBack(pmBody);
          }

          break;
        }
      }

    };

  //#endregion |-----------------------------------------------------------
  
  //#region - Conecta na impressora |--------------------------------------

    lnkPrn = "ws"+window.location.href.slice(4)+"Prn";
    wsPrn = new WebSocket(lnkPrn);  

    wsPrn.onopen = () => {
    };
    
    wsPrn.onerror = (err) => {
      console.log(`[SysInfo] Printer Error:\n${err}`);
    };
    
    wsPrn.onmessage = async (evt) => {  
                
      const data = evt.data;

      await ptCallBack({Call:"Printer",Args:data}); //Precisa enviar o dado direto pois é buffer

    };    

  //#endregion |-----------------------------------------------------------

  return new Promise( function(resolve, reject) {
    glwsCtrRes = resolve;
    glwsCtrRej = reject;
  });

}

//#region - Chamadas dos Sockets |-----------------------------------------

async function WebApi(pmBody) {

  if( (pmBody.Call == undefined) || (pmBody.Args == undefined) ) {
    return({Call:"NAK",Args:"[PMU] Parametros Não Definidos"});
  }

  let lcCont = 3000; 
  let lcFlag = 0;
  
  return new Promise( function(resolve, reject) {

    //wsQry.push()
        
    ( function WaitToSend() {

      if( (lcFlag == 0) && (wsRet == null) ) lcFlag = 1;
      
      if( lcFlag == 1) {
        wsRet = {Call:pmBody.Call, Args:pmBody.Args, Body: null}; //Protecao para caso recebe algum comando durante o aguardo desse        
        wsCtr.send( JSON.stringify(pmBody) );
        lcFlag = 2;
      }

      if( lcCont > 0) {

        if( (lcFlag == 2) && (wsRet.Body != null) ){
          resolve(wsRet.Body);
          wsRet=null;
        } else {
          lcCont = lcCont - 100;
          setTimeout( WaitToSend,100);
        }
      }

      else {
        wsRet=null;             //Zera, mesmo que de outro, pra nao travar o sistema
        if( lcFlag == 0) {
          resolve({Call:`NAK`, Args:`[WCO] Canal de comunicação ocupado`});          
        }
        else{
          resolve({Call:`NAK`, Args:`[WTO] Comando não respondeu`});
        }
      }
          

    })();

  });

}

async function WebPrn( pmData ) {

  await wsPrn.send(pmData);  

}

//#endregion |-----------------------------------------------------------
